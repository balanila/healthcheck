![](assets/title.png)

[![GoDoc](https://godoc.org/gitlab.com/rakshazi/healthcheck?status.svg)](https://godoc.org/gitlab.com/rakshazi/healthcheck)
[![Patreon](https://img.shields.io/badge/donate-patreon-orange.svg?style=for-the-badge)](https://patreon.com/rakshazi)
[![PayPal](https://img.shields.io/badge/donate-paypal-blue.svg?style=for-the-badge)](https://paypal.me/rakshazi)
[![Liberapay](https://img.shields.io/badge/donate-liberapay-yellow.svg?style=for-the-badge)](https://liberapay.com/rakshazi)



This program will constantly check for one or more http(-s), websocket or redis endpoints and notify you
 whenever something goes down and also when it goes up again!
It will additionally expose an api with the current statuses of every endpoint
configured.

Example response:

```json
{
  "providers": [
    {
      "name": "Google",
      "url": "https://www.google.com",
      "status": true,
      "responce_time": 103,
      "code": 200,
      "ready": true
    },
    {
      "name": "WebSocket Echo",
      "url": "wss://echo.websocket.org",
      "status": true,
      "responce_time": 550,
      "code": 200,
      "ready": true
    },
    {
      "name": "Redis test",
      "url": "redis://localhost:6379/0",
      "status": true,
      "responce_time": 7,
      "code": 200,
      "ready": true
    }
  ]
}

```

## Configuration

This health check needs a `config.json` file with this minimal structure

```json
{
  "urlMonitors": [
        {
            "name": "Google",
            "url": "https://www.google.com"
        },
        {
            "name": "WebSocket Echo",
            "url": "wss://echo.websocket.org"
        },
        {
            "name": "Redis test",
            "url": "redis://localhost:6379/0"
        }
  ]
}
```

You can also specify the config file path with the `-config` flag. For example:

```bash
healthcheck -config my/custom/dir/my_config.json
```

Check more configuration options in the [conf.json.dist](config.json.dist)

<div align="center">
<img src="assets/uml.png" alt="Features diagram" title="Features diagram">
</div>

## Run

### Golang

```bash
go get gitlab.com/rakshazi/healthcheck
cd $GOPATH/src/gitlab.com/rakshazi/healthcheck
go install
curl https://gitlab.com/rakshazi/healthcheck/raw/master/config.json.dist -o config.json
healthcheck
```

### Binary

```bash
# release 0.1.2 from project -> releases
curl https://gitlab.com/rakshazi/healthcheck/uploads/1dd82551e81428947c9ae80e35d0b0bd/healthcheck_amd64_linux healthcheck
curl https://gitlab.com/rakshazi/healthcheck/raw/master/config.json.dist -o config.json
chmod +x ./healthcheck
healthcheck
```

### Docker

```bash
curl https://gitlab.com/rakshazi/healthcheck/raw/master/config.json.dist -o config.json
docker run -d -v "$PWD/config.json:/etc/monitor.json" -p 80:8080 registry.gitlab.com/rakshazi/healthcheck

curl http://localhost
```

## Fork differencies

> This repository is fork of original [Healtch Check Monitor](https://gitlab.com/skyvet/health-check-monitor)

### Status report

1. Removed fields: `version`, `environment`
2. Added field `code` for each provider with HTTP response code from last check
3. **IF** anyone of providers will go down, status report will return HTTP status code `503`, otherwise - `200`

### Config

1. Default port changed to `8080`
2. Default endpoint changed to `/`

### Code structure

1. Renamed `main.go` files in subpackages

### Docker

1. Moved to multi-stage builds
2. Updated compiler to latest stable version
3. Used `scratch` as base image for compiled app
4. Used non-privileged docker user for additional security

### ...and more

* WebSocket checks
* Redis checks
* GoDoc reference
* Telegram listener
* Response time measurements
* ...
