package checker

import (
	"fmt"
	"log"
	"net/http"
	"runtime"
	"strings"
	"time"

	"gitlab.com/rakshazi/healthcheck/config"
	"gitlab.com/rakshazi/healthcheck/listener"
	. "gitlab.com/rakshazi/healthcheck/monitor"
)

var statusString = map[bool]string{
	true:  "up",
	false: "down",
}

var validStatusCodes = map[int]bool{
	http.StatusCreated: true,
	http.StatusOK:      true,
}

type Checker struct {
	Config      config.Config
	URLMonitors []*URLMonitor
	Listeners   []listener.StatusChangeListener
}

func (c *Checker) RegisterProvider(URLMonitor *URLMonitor) error {
	if URLMonitor.Name == "" {
		return fmt.Errorf("monitor is missing name")
	}

	if URLMonitor.URL == "" {
		return fmt.Errorf("monitor %s is missing url", URLMonitor.Name)
	}

	c.URLMonitors = append(c.URLMonitors, URLMonitor)

	return nil
}

func (c *Checker) RegisterListener(l listener.StatusChangeListener) error {
	if err := l.OnRegister(); err != nil {
		return fmt.Errorf("could not register listener: %v", err)
	}

	c.Listeners = append(c.Listeners, l)
	log.Println("listener registered")

	return nil
}

func (c *Checker) Run() {
	checkTicker := time.NewTicker(time.Second * time.Duration(c.Config.Checker.Interval))
	gcTicker := time.NewTicker(time.Second * time.Duration(c.Config.Checker.Interval) * 2)
	defer checkTicker.Stop()
	defer gcTicker.Stop()
	for {
		select {
		case <-checkTicker.C:
			c.checkAll()
		case <-gcTicker.C:
			runtime.GC()
		}
	}
}

func (c *Checker) checkAll() {

	for _, urlMonitor := range c.URLMonitors {
		resp, ttfb, err := c.fetchMonitorURL(urlMonitor)

		status := true

		if err != nil {
			log.Printf("Error on request for monitor %s: %v", urlMonitor.Name, err)
			status = false
		}

		if bool(!validStatusCodes[resp]) {
			status = false
		}

		urlMonitor.PushStatus(status, ttfb, resp)

		if urlMonitor.StatusChanged() && urlMonitor.IsReady() {
			currentStatus, ttfb, currentCode, _ := urlMonitor.GetCurrentStatus()
			log.Printf("%s monitor is now %s (HTTP %d, TTFB %d)", urlMonitor.Name, statusString[currentStatus], currentCode, ttfb)

			for _, l := range c.Listeners {
				go l.OnStatusChange(urlMonitor)
			}
		}
	}
}

func (c *Checker) fetchMonitorURL(URLMonitor *URLMonitor) (int, int, error) {
	switch {
	case strings.HasPrefix(URLMonitor.URL, "ws://") || strings.HasPrefix(URLMonitor.URL, "wss://"):
		return checkWebsocket(URLMonitor)
	case strings.HasPrefix(URLMonitor.URL, "redis://"):
		return checkRedis(URLMonitor)
	default:
		return checkHttp(URLMonitor, time.Second*time.Duration(c.Config.Checker.Timeout))
	}
}
