package monitor

import (
	"encoding/json"
)

const THRESHOLD = 3

// URLMonitor This struct handles status of each url
type URLMonitor struct {
	Name   string
	Status []bool
	Ttfb   int
	Code   int
	URL    string
}

func (monitor *URLMonitor) PushStatus(status bool, ttfb int, code int) {
	monitor.Status = append(monitor.Status, status)
	monitor.Ttfb = ttfb
	monitor.Code = code

	if len(monitor.Status) > THRESHOLD {
		monitor.Status = monitor.Status[1:]
	}
}

func (monitor *URLMonitor) StatusChanged() bool {
	// If not enough statuses pushed, just return false
	if !monitor.IsReady() {
		return false
	}

	lastStatus := monitor.Status[0]
	otherStatuses := monitor.Status[1:]

	// Check if status is changing
	for _, status := range otherStatuses {
		if status != otherStatuses[0] {
			return false
		}
	}

	// Only changed when status stack is stable
	return otherStatuses[0] != lastStatus
}

func (monitor *URLMonitor) GetCurrentStatus() (bool, int, int, bool) {
	if !monitor.IsReady() {
		return false, 0, 0, false
	}

	return monitor.Status[len(monitor.Status)-1], monitor.Ttfb, monitor.Code, true
}

func (monitor *URLMonitor) IsReady() bool {
	return len(monitor.Status) == THRESHOLD
}

func (monitor *URLMonitor) MarshalJSON() ([]byte, error) {
	status, ttfb, code, ready := monitor.GetCurrentStatus()

	return json.Marshal(struct {
		Name   string `json:"name"`
		URL    string `json:"url"`
		Status bool   `json:"status"`
		Ttfb   int    `json:"responce_time"`
		Code   int    `json:"code"`
		Ready  bool   `json:"ready"`
	}{
		Name:   monitor.Name,
		Status: status,
		Ttfb:   ttfb,
		Code:   code,
		URL:    monitor.URL,
		Ready:  ready,
	})
}
